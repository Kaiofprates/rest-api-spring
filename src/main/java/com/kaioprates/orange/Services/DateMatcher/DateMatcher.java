package com.kaioprates.orange.Services.DateMatcher;

public interface DateMatcher {
    boolean matches(String date);
}
