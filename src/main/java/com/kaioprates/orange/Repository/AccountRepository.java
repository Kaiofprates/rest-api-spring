package com.kaioprates.orange.Repository;

import com.kaioprates.orange.Models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository <Account, Long> {
    List <Account> findByEmailOrCpf(String email, String cpf);
}
