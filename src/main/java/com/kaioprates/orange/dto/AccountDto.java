package com.kaioprates.orange.dto;

import com.kaioprates.orange.Models.Account;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Getter
@Setter
public class AccountDto {
    @NotNull
    @NotBlank
    private String name;

    @NotBlank
    @Length(max = 11,min = 11,message = "The cpf field must contain 11 numbers")
    private String cpf;

    @NotBlank
    @DateTimeFormat()
    public String birth;

    @NotBlank
    @Email(message = "Enter a valid email")
    private String email;

    public Account parseToAccount(){
        return new Account(name,email,cpf,birth);
    }
}
