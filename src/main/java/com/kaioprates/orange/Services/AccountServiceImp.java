package com.kaioprates.orange.Services;

import com.kaioprates.orange.Models.Account;
import com.kaioprates.orange.Repository.AccountRepository;
import com.kaioprates.orange.Services.DateMatcher.DateMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service

public class AccountServiceImp implements AccountService{
    private final AccountRepository repository;
    private  final DateMatcher dateMatcher;

    @Autowired
    public AccountServiceImp(AccountRepository repository, DateMatcher dateMatcher) {
        this.repository = repository;
        this.dateMatcher = dateMatcher;
    }

    @Override
    public List<Account> findAll() {
        return repository.findAll();
    }

    @Override
    public Account save(Account account) {

        boolean date = dateMatcher.matches(account.getBirth());
        Assert.isTrue(date,"The birth field must follow the yyyy-MM-dd pattern");

        List<Account> duplicate = repository.findByEmailOrCpf(account.getEmail(),account.getCpf());
        Assert.isTrue(duplicate.isEmpty(), "Duplicate fields exists");

        return repository.save(account);
    }

    @Override
    public void delete(Long id) {
        try {
            repository.deleteById(id);
        }catch (Exception ignored) {
        }
    }

    @Override
    public Account update(Account account, Long id) {

        Account updated = repository.findById(id)
                .map(up -> {
                    up.setName(account.getName());
                    up.setEmail(account.getEmail());
                    up.setBirth(account.getBirth());
                    up.setCpf(account.getCpf());
                    Account data = repository.save(up);
                    return data;
                }).get();

        return updated;
    }

    @Override
    public Account findById(Long id) {
        return repository.findById(id).get();
    }
}
