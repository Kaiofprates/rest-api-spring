package com.kaioprates.orange;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaioprates.orange.Models.Account;
import com.kaioprates.orange.Repository.AccountRepository;
import com.kaioprates.orange.dto.AccountDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("dev")

public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AccountRepository accountRepository;

    private AccountDto accountTest(){
        AccountDto data  = new AccountDto();
        data.setBirth("1963-11-22");
        data.setCpf("12345678912");
        data.setEmail("testemail@gmail.com");
        data.setName("John Doe");
        return data;
    }

    @Test
    public void testCreate() throws Exception {
        accountRepository.deleteAll();
        AccountDto data = accountTest();

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(data))
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.name").value(data.getName()))
                .andExpect(jsonPath("$.email").value(data.getEmail()))
                .andExpect(jsonPath("$.cpf").value(data.getCpf()))
                .andExpect(jsonPath("$.birth").value(data.getBirth()))
                .andDo(MockMvcResultHandlers.print());

        Assertions.assertFalse(accountRepository.findAll().isEmpty());
    }

    @Test
    public void testFindByid() throws Exception {
        Account result = accountRepository.save(accountTest().parseToAccount());
        Long id = result.getId();
        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/"+id.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.email").isString())
                .andExpect(jsonPath("$.cpf").isString())
                .andExpect(jsonPath("$.birth").isString())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testFindAll() throws Exception {
        Account data = accountTest().parseToAccount();
        accountRepository.save(data);

        mockMvc.perform(MockMvcRequestBuilders.get("/accounts"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect((jsonPath("$").isArray()))
                .andExpect((jsonPath("$[0].name").isString()))
                .andExpect((jsonPath("$[0].email").isString()))
                .andExpect((jsonPath("$[0].cpf").isString()))
                .andExpect((jsonPath("$[0].birth").isString()))
                .andDo(MockMvcResultHandlers.print());
    }
    @Test
    public void testDelete() throws Exception{
        Account data = accountRepository.save(accountTest().parseToAccount());
        Long id = data.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete("/accounts/" + id.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

        Optional<Account> search = accountRepository.findById(id);
        Assertions.assertFalse(search.isPresent());
    }

    @Test
    public void  duplicateEmailOrCpfTest() throws  Exception {

        AccountDto data  = accountTest();
        accountRepository.save(data.parseToAccount());

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(data.parseToAccount()))
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.statusCode").isNumber())
                .andExpect(jsonPath("$.message").isString())
                .andExpect(jsonPath("$.statusCode").value(400))
                .andExpect(jsonPath("$.message").value("Duplicate fields exists"))
                .andDo(MockMvcResultHandlers.print());
    }
    @Test
    public void  invalidFormatDateTest() throws  Exception {

        Account data  = accountTest().parseToAccount();
        data.setBirth("1963-18-123");

        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(data))
        ).andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(jsonPath("$.statusCode").isNumber())
                .andExpect(jsonPath("$.message").isString())
                .andExpect(jsonPath("$.statusCode").value(400))
                .andExpect(jsonPath("$.message").value("The birth field must follow the yyyy-MM-dd pattern"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdate() throws Exception {
        accountRepository.deleteAll();
        Account data  = accountTest().parseToAccount();
        // salva dados de teste
        Account result = accountRepository.save(data);
        // atualiza o campo email
        data.setEmail("test2@gmail.com");
        // pegamos o campo id
        Long id  = result.getId();

        mockMvc.perform(MockMvcRequestBuilders.put("/accounts/"+id.toString())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(data))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.name").value(data.getName()))
                .andExpect(jsonPath("$.email").value(data.getEmail()))
                .andExpect(jsonPath("$.cpf").value(data.getCpf()))
                .andExpect(jsonPath("$.birth").value(data.getBirth()))
                .andDo(MockMvcResultHandlers.print());

        Optional<Account> search = accountRepository.findById(id);
        Assertions.assertTrue(search.isPresent());
        Assertions.assertEquals(data.getEmail(),search.get().getEmail());
    }


}
