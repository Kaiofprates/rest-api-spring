package com.kaioprates.orange.Services;

import com.kaioprates.orange.Models.Account;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    List<Account> findAll();
    Account save(Account account);
    void delete(Long id);
    Account update(Account account, Long id);
    Account findById(Long id);
}
