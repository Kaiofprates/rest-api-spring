package com.kaioprates.orange.Controllers;

import com.kaioprates.orange.Models.Account;
import com.kaioprates.orange.Repository.AccountRepository;
import com.kaioprates.orange.Services.AccountService;
import com.kaioprates.orange.dto.AccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping({"/accounts"})
//@Profile( value = {"prod","dev"})

public class AccountController {

    private final AccountService service;

    @Autowired
    public AccountController(AccountRepository accountRepository, AccountService service) {
        this.service = service;
    }

    // Lista todos os Registros
    @GetMapping
    public List<Account> findAll(){
        return service.findAll();
    }

    // Cria Novos registros
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Account create(@RequestBody @Valid AccountDto dto){

        return service.save(dto.parseToAccount());
    }

    // Atualiza registros
    @PutMapping(value = "/{id}")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody @Valid AccountDto dto){

       try {
           Account data = service.update(dto.parseToAccount(), id);
           return ResponseEntity.ok(data);
       }catch (NoSuchElementException exception){
           return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
       }
    }

    // Busca registros pelo id
    @GetMapping(path = "/{id}")
    public ResponseEntity findById(@PathVariable long id){

        try{
            Account data =  service.findById(id);
            return ResponseEntity.ok(data);
        }catch (NoSuchElementException exception) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

    }

    // Deleta um registro pelo id
    @DeleteMapping (path = "/{id}")
    public void delete(@PathVariable long id){
     service.delete(id);
    }
}
